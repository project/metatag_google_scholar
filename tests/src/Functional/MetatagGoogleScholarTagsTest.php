<?php

namespace Drupal\Tests\metatag_google_scholar\Functional;

use Drupal\Tests\metatag\Functional\MetatagTagsTestBase;

/**
 * Tests that each of the Metatag Google Scholar tags work correctly.
 *
 * @group metatag
 */
class MetatagGoogleScholarTagsTest extends MetatagTagsTestBase {

  /**
   * {@inheritdoc}
   */
  protected $tags = [
    'citation_title',
    'citation_author',
    'citation_publication_date',
    'citation_journal_title',
    'citation_issn',
    'citation_isbn',
    'citation_volume',
    'citation_issue',
    'citation_firstpage',
    'citation_lastpage',
    'citation_dissertation_institution',
    'citation_technical_report_institution',
    'citation_technical_report_number',
    'citation_pdf_url',
  ];

  /**
   * {@inheritdoc}
   *
   * @todo Make the property protected once it's protected in the base class.
   * @see https://www.drupal.org/node/2909426
   */
  public static $modules = ['metatag_google_scholar'];

  /**
   * Implements {meta_tag_name}TestValue() for 'citation_pdf_url'.
   */
  protected function citationPdfUrlTestValue() {
    return 'https://www.example.com/documents/' . $this->randomMachineName() . '.pdf';
  }

}
